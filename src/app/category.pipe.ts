import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'category'
})
export class CategoryPipe implements PipeTransform {

  transform(electronics: any, propertyName: string, searchStr2: string): any {

    let selectedcate = [];

    if (electronics.length === 0 || searchStr2 == null) { 
      return electronics;
    }

   

    selectedcate = electronics.filter(electronic => { 
      return electronic.categoryID.indexOf(searchStr2) >= 0 
    });

    return selectedcate;
  }

}
