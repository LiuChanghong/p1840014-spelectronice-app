import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService} from "../auth/auth.service"

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {
  emailBlackList = ['test@test.com', 'temp@temp.com'];

  contactForm: FormGroup;
  submitted = false;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      'userID'   : new FormControl(null, [Validators.required, this.blankSpaces]),
      'userPass'   : new FormControl(null, [Validators.required, this.blankSpaces]),
      
    });

  }

  blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }
  onSubmit() {
    console.log(this.contactForm);
    this.submitted = true;
  }

  onLogin(userID: HTMLInputElement, userPass: HTMLInputElement){

    this.authService.login(userID.value, userPass.value);
  }
}
