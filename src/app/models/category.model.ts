export class Category {

    public categoryID: string;
    public categoryname: string;
    public categorydescription: string;


    constructor(categoryID: string, categoryname: string, categorydescription: string){

         this.categoryID = categoryID;
         this.categoryname = categoryname;
         this.categorydescription = categorydescription;

    }


}