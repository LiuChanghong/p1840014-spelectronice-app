import { Component, OnInit, Input } from '@angular/core';
import { Enquiry } from '../../models/enquiry.model';

import { Router, ActivatedRoute } from '@angular/router';
import { EnquiryService } from "../../services/enquiry.service";


@Component({
  selector: 'app-enquiry-detail',
  templateUrl: './enquiry-detail.component.html',
  styleUrls: ['./enquiry-detail.component.css']
})
export class EnquiryDetailComponent implements OnInit {

 

  chosenenquiry: Enquiry = new Enquiry("","","","","");
 
  selectedEnquiry: Enquiry = new Enquiry('1','ajjwdhu@enquiry.com','my ting is the ting got ting','faiyuhufjvggsgskbhfegshubqrhueugshuqor ugisuqreguqreugeuqehguhsiehgkbkiqrohehkgbriohwgrhbjoroiwgrk','ususus');


  constructor(private enquiryService: EnquiryService, public router: Router, private Route: ActivatedRoute,public route: ActivatedRoute) { }

  ngOnInit() {

    const enquiryusername = this.route.snapshot.params['enquiryusername'];

    console.log(`The enquiry is ${enquiryusername}`);

    const searchResult = this.enquiryService.getEnquiry(enquiryusername);

    console.log(searchResult+"is searchresult");
    
    if (searchResult != undefined) {
      this.selectedEnquiry = searchResult;
    } else {
      this.router.navigate(['/not-found'])
      alert("something wrong")
    }


  }

  onUpdate( enquiryemail: string, enquirysubject: string,enquirymessage:string, enquirystatus:string) {
  
    let updateInfo = new Enquiry(this.selectedEnquiry.enquiryusername, enquiryemail, enquirysubject, enquirymessage,enquirystatus);
    this.enquiryService.updateEnquiry(updateInfo).subscribe(
      (success) => {
       
        if (success) {
         
     /* alert("updated");*/
  
     
     
        } else {
       
        }
      }
    );
   
    
  
  }

}
