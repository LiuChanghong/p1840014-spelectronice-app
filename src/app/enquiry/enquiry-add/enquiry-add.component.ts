import { Component, OnInit } from '@angular/core';
import { Enquiry } from "../../models/enquiry.model";
import { EnquiryService } from "../../services/enquiry.service";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-enquiry-add',
  templateUrl: './enquiry-add.component.html',
  styleUrls: ['./enquiry-add.component.css']
})
export class EnquiryAddComponent implements OnInit {

  emailBlackList = ['test@test.com', 'temp@temp.com'];

  submitted = false;

  inputInfo2 : Enquiry = new Enquiry("","","","","")

  constructor(private enquiryService : EnquiryService) { }

  EnquiryForm: FormGroup;
  
  ngOnInit() {
    this.EnquiryForm = new FormGroup({
      
      'EnquiryEmail'  : new FormControl(null, [Validators.required, Validators.email, this.blankSpaces, this.inEmailBlackList.bind(this)]),
      'EnquirySubject': new FormControl(null, [Validators.required, this.blankSpaces]),
      'EnquiryMessage': new FormControl(null, [Validators.required, this.blankSpaces])
      
    });
  }

   
  blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }

  inEmailBlackList(control: FormControl): {[s: string]: boolean} {
    if (this.emailBlackList.indexOf(control.value) !== -1) {
      return {'emailBlackListed': true};
    }
    return null;
  }

  onAddEnquiry(EnquiryName:HTMLInputElement,
    EnquiryEmail:HTMLInputElement,
    EnquirySubject:HTMLInputElement,
    EnquiryMessage:HTMLInputElement,
    EnquiryStatus:HTMLInputElement
    ){

      if (EnquiryEmail.value == ""||EnquirySubject.value == ""||EnquiryMessage.value == "") {
        alert ("Wrong or empty Information, please try again!")
        return false;
      } else
      
       
        this.enquiryService.addEnquiry(new Enquiry(
          EnquiryName.value,
          EnquiryEmail.value,
          EnquirySubject.value,
          EnquiryMessage.value,
          EnquiryStatus.value,
        ));
       alert("Enquiry submitted successfully")

   


  }
  onSubmit() {
    console.log(this.EnquiryForm);
    this.submitted = true;
  }

  
  
}
