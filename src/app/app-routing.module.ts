import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ElectronicComponent } from './electronic/electronic.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ElectronicDetailComponent } from './electronic/electronic-detail/electronic-detail.component';
import { EnquiryDetailComponent } from './enquiry/enquiry-detail/enquiry-detail.component';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';

import { AuthGuardService } from './auth/auth-guard.service';


const routes: Routes = [
  {path:'', redirectTo:'/home',pathMatch:'full'},
  {path:'home',component:HomeComponent},
  {path:'electronic', canActivate:[AuthGuardService], component:ElectronicComponent},
  {path:'contactus', component:ContactUsComponent},
  {path:'enquiry', component:EnquiryComponent},
  {path:'adminlogin', component:AdminloginComponent},
  {path:'not-found', component:PageNotFoundComponent},
  {path:'details/:productID',component:ElectronicDetailComponent},
  {path:'enquirydetails/:enquiryusername',component:EnquiryDetailComponent},
 
  {path:'**', redirectTo:'/not-found'},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
