import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { ReactiveFormsModule } from "@angular/forms"
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ElectronicComponent } from './electronic/electronic.component';
import { ElectronicAddComponent } from './electronic/electronic-add/electronic-add.component';
import { ElectronicListComponent } from './electronic/electronic-list/electronic-list.component';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HeaderComponent } from './header/header.component';
import { ElectronicDetailComponent } from './electronic/electronic-detail/electronic-detail.component';
import { SearchPipe } from './search.pipe';
import { CategoryPipe } from './category.pipe';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { EnquiryListComponent } from './enquiry/enquiry-list/enquiry-list.component';
import { EnquiryAddComponent } from './enquiry/enquiry-add/enquiry-add.component';
import { AuthService } from './auth/auth.service'
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { HoverHighlightDirective } from './hover-highlight.directive';
import { HoverHighlightgreenDirective } from './hover-highlightgreen.directive';
import { HoverHighlightwhiteDirective } from './hover-highlightwhite.directive';
import { EnquiryDetailComponent } from './enquiry/enquiry-detail/enquiry-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    ElectronicComponent,
    ElectronicAddComponent,
    ElectronicListComponent,
    HomeComponent,
    ContactUsComponent,
    PageNotFoundComponent,
    HeaderComponent,
    ElectronicDetailComponent,
    SearchPipe,
    CategoryPipe,
    EnquiryComponent,
    EnquiryListComponent,
    EnquiryAddComponent,
    AdminloginComponent,
    HoverHighlightDirective,
    HoverHighlightgreenDirective,
    HoverHighlightwhiteDirective,
    EnquiryDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
