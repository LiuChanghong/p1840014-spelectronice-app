import { Component, OnInit,Input } from '@angular/core';
import { Electronic } from '../../models/electronic.model'
import { Category } from '../../models/category.model'
import { Router, ActivatedRoute } from '@angular/router';
import { ElectronicsService } from "../../services/electronics.service";
import { CategoryService } from "../../services/category.service";

@Component({
  selector: 'app-electronic-detail',
  templateUrl: './electronic-detail.component.html',
  styleUrls: ['./electronic-detail.component.css']
})
export class ElectronicDetailComponent implements OnInit {

  showEditForm: boolean = false; 

  @Input() myCategory: Category[] = [];

  chosenproduct: Electronic = new Electronic("","","","","","","");
  chosencategory: Category = new Category("","","")
  categoryList: Category[] = [];

  selectedElectronic: Electronic = new Electronic('1','Zephyrus M','Thin and Portable Gaming Laptop, 15.6” 240Hz FHD IPS, NVIDIA GeForce RTX 2070, Intel Core i7-9750H, 16GB DDR4 RAM, 1TB PCIe SSD, Per-Key RGB, Windows 10 Home, GU502GW-AH76','ROG','$1,849.99','1','./assets/img/ZephyrusM.jpg');

  constructor(private electronicsService: ElectronicsService,private categoryService: CategoryService, public router: Router, private Route: ActivatedRoute,public route: ActivatedRoute) { }

  

  ngOnInit() {
    /*
    this.chosenproduct = this.electronicsService.getChosenElectronics(this.Route.snapshot.params['productID'] );
    console.log(this.chosenproduct);

*/
const productID = this.route.snapshot.params['productID'];

    console.log(`The productID is ${productID}`);

    const searchResult = this.electronicsService.getElectronic(productID);

    console.log(searchResult+"is searchresult");

    if (searchResult != undefined) {
      this.selectedElectronic = searchResult;
    } else {
      this.router.navigate(['/not-found'])
      alert("something wrong");
    }

  }
  onUpdate(name: string, description: string, brand: string, price:string, categoryID:string,imageURL:string ) {
    let updateInfo = new Electronic(this.selectedElectronic.productID, name, description, brand,price,categoryID,imageURL);
    this.electronicsService.updateElectronic(updateInfo).subscribe(
      (success) => {
        this.toggleShowEditForm();
        if (success) {
          alert("Electronic information Updated");
          this.selectedElectronic.name    = updateInfo.name; 
          this.selectedElectronic.description   = updateInfo.description; 
          this.selectedElectronic.brand = updateInfo.brand; 
          this.selectedElectronic.price = updateInfo.price; 
          this.selectedElectronic.categoryID = updateInfo.categoryID; 
          this.selectedElectronic.imageURL = updateInfo.imageURL; 
        } else {
          alert("Update Failed, Please try again.");
        }
      }
    );
  }

  toggleShowEditForm() {
    this.showEditForm = !this.showEditForm;
  }
}
