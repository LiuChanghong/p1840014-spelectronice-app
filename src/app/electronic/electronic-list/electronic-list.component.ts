import { Component, OnInit, Input } from '@angular/core';
import { Electronic } from "../../models/electronic.model";
import { ElectronicsService } from "../../services/electronics.service";
import { CategoryService } from "../../services/category.service";
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/models/category.model';


@Component({
  selector: 'app-electronic-list',
  templateUrl: './electronic-list.component.html',
  styleUrls: ['./electronic-list.component.css']
})
export class ElectronicListComponent implements OnInit {

  @Input() myElectronics : Electronic [];
  @Input() myCategory: Category[] = [];

  constructor(private router: Router, public electronicsService: ElectronicsService, private route: ActivatedRoute, private categoryService: CategoryService) { }

  ngOnInit() {
  }
  
  viewdetails(productID:string){
    console.log(productID);
    this.router.navigate(['/details', productID])

  }

  onDelete(productID:string){
    console.log(productID);
    this.electronicsService.removeElectronic(productID);
  }

  
}

