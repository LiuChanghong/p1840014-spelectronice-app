import { Component, OnInit } from '@angular/core';
import { Electronic } from "../models/electronic.model";
import { Category } from "../models/category.model";

import { ElectronicsService } from "../services/electronics.service";
import { CategoryService } from "../services/category.service";

@Component({
  selector: 'app-electronic',
  templateUrl: './electronic.component.html',
  styleUrls: ['./electronic.component.css']
  //providers: [ElectronicsService,CategoryService]
})
export class ElectronicComponent implements OnInit {

  categoryList : Category [] = [
    /* new Category('1','laptop','portable computer')*/
   ];

  electronicList : Electronic [] = [
   /* new Electronic('1','Laptop','this is a normal laptop','Alienware','$20','1','image11.png')*/
  ];
  
  constructor(private electronicsService: ElectronicsService,private categoryService: CategoryService) { }


  ngOnInit() {
    
    /*
    this.electronicList = this.electronicsService.getElectronics();

*/
this.electronicsService.loadElectronic()
.subscribe( (result)=> { 
  this.electronicList = this.electronicsService.getElectronics();
} );


this.categoryService.loadCategory()
.subscribe( (result)=> { 
  this.categoryList = this.categoryService.getCategories();
} );


    this.categoryList = this.categoryService.getCategories();

    this.electronicsService.electronicListUpdated.
    subscribe(() =>{
      this.electronicList = this.electronicsService.getElectronics();
    })
    
  }

  onelectronicListUpdated(newElectronicInfo){

    console.log(newElectronicInfo);
    this.electronicList.push(newElectronicInfo);
    console.log(this.electronicList);
    console.log('it works');
    
  }

  
    showadd(){
    var x = document.getElementById("addelectronic");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }
 

}


