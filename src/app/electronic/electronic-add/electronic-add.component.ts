import { Component, OnInit /*, ElementRef, ViewChild, EventEmitter, Output*/} from '@angular/core';
import { Electronic } from "../../models/electronic.model";
import { ElectronicsService } from "../../services/electronics.service";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-electronic-add',
  templateUrl: './electronic-add.component.html',
  styleUrls: ['./electronic-add.component.css']
})
export class ElectronicAddComponent implements OnInit {

  emailBlackList = ['test@test.com', 'temp@temp.com'];

  submitted = false;

  ElectronicForm: FormGroup;

  inputInfo : Electronic = new Electronic("","","","","","","")
/*
  @ViewChild('inputProductID',{static:false}) inputProductID: ElementRef;
  @ViewChild('inputName',{static:false}) inputName: ElementRef;
  @ViewChild('inputDescription',{static:false}) inputDescription: ElementRef;
  @ViewChild('inputBrand',{static:false}) inputBrand: ElementRef;
  @ViewChild('inputPrice',{static:false}) inputPrice: ElementRef;
  @ViewChild('inputCategoryID',{static:false}) inputCategoryID: ElementRef;
  @ViewChild('inputImageURL',{static:false}) inputImageURL: ElementRef;

  @Output() electronicListUpdated = new EventEmitter<Electronic>();
*/
  constructor(private electronicsService : ElectronicsService) { }

  ngOnInit() {
    this.ElectronicForm = new FormGroup({
     
      'inputName'  : new FormControl(null, [Validators.required, this.blankSpaces]),
      'inputDescription': new FormControl(null, [Validators.required, this.blankSpaces]),
      'inputBrand': new FormControl(null, [Validators.required, this.blankSpaces]),
      'inputPrice': new FormControl(null, [Validators.required, this.blankSpaces]),
     /* 'inputCategoryID': new FormControl(null, [Validators.required, this.blankSpaces]),*/
      'inputImageURL': new FormControl(null, [Validators.required, this.blankSpaces])
      
    });
  }

  blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }

  inEmailBlackList(control: FormControl): {[s: string]: boolean} {
    if (this.emailBlackList.indexOf(control.value) !== -1) {
      return {'emailBlackListed': true};
    }
    return null;
  }

  onSubmit() {
    console.log(this.ElectronicForm);
    this.submitted = true;
  }

  
  onAddElectronic(inputProductID:HTMLInputElement,
    inputName:HTMLInputElement,
    inputDescription:HTMLInputElement,
    inputBrand:HTMLInputElement,
    inputPrice:HTMLInputElement,
    inputCategoryID:HTMLInputElement,
    inputImageURL:HTMLInputElement){
 /*
    console.log("add electronic");
    console.log(this.inputInfo);
    console.log(this.inputProductID.nativeElement.value);
    console.log(this.inputName.nativeElement.value);
    console.log(this.inputDescription.nativeElement.value);
    console.log(this.inputBrand.nativeElement.value);
    console.log(this.inputPrice.nativeElement.value);
    console.log(this.inputCategoryID.nativeElement.value);
    console.log(this.inputImageURL.nativeElement.value);
  */
 /*console.log(inputDescription.value);
  this.electronicListUpdated.emit(new Electronic(
      inputProductID.value,
      inputName.value,
      inputDescription.value,
      inputBrand.value,
      inputPrice.value,
      inputCategoryID.value,
      inputImageURL.value


    ));  
    */
   if (inputName.value == ""||inputDescription.value == ""||inputBrand.value == ""||inputPrice.value == ""||inputCategoryID.value == ""||inputImageURL.value == "") {
    alert ("Wrong or empty Information, please try again!")
    return false;
  } else
    this.electronicsService.addElectronic(new Electronic(
      inputProductID.value,
      inputName.value,
      inputDescription.value,
      inputBrand.value,
      inputPrice.value,
      inputCategoryID.value,
      inputImageURL.value
    ));
    alert("New Electronic Added")
  }

}
