import { EventEmitter,Injectable } from '@angular/core';
import { Category } from '../models/category.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from "rxjs/operators";

import { environment } from "../../environments/environment";

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class CategoryService {


  private categoryList : Category [] = [/*
     new Category('1','Laptop','Some sort of portable computer.'),
     new Category('2','Telephones','A portable communication tool.'),
     new Category('3','Earphones','A portable speaker where you can put in your year.'),
*/
   ];

   getCategoryName(categoryID){ 
    for(let c of this.categoryList){
      if(c.categoryID == categoryID){  
        return c.categoryname; 
      }
    }
    return "not found;"
  }

  

  constructor(public httpClient: HttpClient) { }

  addCategory(newCategoryInfo){

    this.categoryList.push(newCategoryInfo);
  //  this.electronicListUpdated.emit();

  }


  getCategories(){
    return this.categoryList.slice();
  }

  loadCategory(){
    return this.httpClient.get<Category[]>("http://localhost:3000/api/Category"/*${APIEndpoint}/api/Electronic */)
    .pipe(map( (Category)=>{
  this.categoryList = Category;
  return Category;
    } , (error)=>{
  
  console.log("error")
    } ));
    }

}
